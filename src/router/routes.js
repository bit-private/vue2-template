import Layout from '@/layout/container.vue'
import Login from '@/views/login/login.vue'

const layoutChildren = [
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/home/index.vue')
  }
]

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    name: 'layout',
    component: Layout,
    redirect: '/home',
    children: layoutChildren
  }
]

export default routes
